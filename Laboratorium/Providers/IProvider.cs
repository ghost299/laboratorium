﻿using System.Collections.ObjectModel;
using Laboratorium.ViewModel;

namespace Laboratorium.Providers
{
    public interface IProvider
    {
        void DBSelectAll(Collection<DBObjectViewModel> list);
        void DBUpdate(Collection<DBObjectViewModel> list);
        string SelectedFile { get; set; }
    }
}
