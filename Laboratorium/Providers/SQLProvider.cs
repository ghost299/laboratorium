﻿using System;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using Laboratorium.ViewModel;

namespace Laboratorium.Providers
{
    public class SQLProvider : IProvider
    {

        public string SelectedFile { get; set; } //TODO: Implementacja

        public void DBSelectAll(Collection<DBObjectViewModel> myList)
        {
            string CS = "data source=LAPTOP-EGC7BKNA\\SQLEXPRESS; database=Diablo; user id=*; password=*; integrated security=SSPI ";
            using (SqlConnection connection = new SqlConnection(CS))
            {
                SqlCommand command = new SqlCommand("SELECT nazwa, cena, ilosc FROM laboratorium", connection);
                myList.Clear();
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    DBObjectViewModel dbObject = new DBObjectViewModel(reader.GetString(0), reader.SafeGetDecimal(1),
                        reader.SafeGetDecimal(2));
                    myList.Add(dbObject);
                }
            }
        }

        public void DBUpdate(Collection<DBObjectViewModel> myRecipe)
        {
            string CS = "data source=LAPTOP-EGC7BKNA\\SQLEXPRESS; database=Diablo; user id=*; password=*; integrated security=SSPI ";
            string name;
            decimal? amount;
            decimal? price;
            using (SqlConnection connection = new SqlConnection(CS))
            {
                connection.Open();
                foreach (var dbObject in myRecipe)
                {
                    name = dbObject.Name;
                    amount = dbObject.Quantity - dbObject.Amount;
                    price = (dbObject.Price/dbObject.Quantity)*amount;
                    SqlCommand command = new SqlCommand("UPDATE laboratorium SET ilosc = @a, cena = @p WHERE nazwa = @i", connection);
                    command.Parameters.Add(ReturnDBParameter("a", amount));
                    command.Parameters.Add(ReturnDBParameter("p", price));
                    command.Parameters.Add(new SqlParameter("i", name));
                    command.ExecuteNonQuery();
                }
            }
        }
        SqlParameter ReturnDBParameter(string parameterName, decimal? value)
        {
            SqlParameter sqlParameter = new SqlParameter(parameterName, Convert.ToDecimal(Math.Round(value.Value, 2)));
            sqlParameter.DbType = DbType.Double;
            return sqlParameter;
        }

    }
}
