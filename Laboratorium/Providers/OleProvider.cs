﻿using System;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.OleDb;
using Laboratorium.ViewModel;

namespace Laboratorium.Providers
{
    public class OleProvider : IProvider
    {
        private static string _selectedFile;
        public string SelectedFile { get { return _selectedFile; } set { _selectedFile = value; } }

        public void DBSelectAll(Collection<DBObjectViewModel> myList)
        {
            if (SelectedFile == null) return;
            string CS = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + SelectedFile + ";" + "Extended Properties='Excel 12.0 Xml;HDR=YES'";
            using (OleDbConnection connection = new OleDbConnection(CS))
            {
                OleDbCommand command = new OleDbCommand("SELECT nazwa, cena, ilość FROM [" + "Arkusz1$" + "]", connection);

                myList.Clear();
                connection.Open();
                OleDbDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    if (reader.IsDBNull(0)) continue; // ignoruje puste wiersze w bazie
                    DBObjectViewModel dbObject = new DBObjectViewModel(reader.GetString(0), reader.SafeGetValue(1), reader.SafeGetValue(2));
                    myList.Add(dbObject);
                }
            }
        }

        public void DBUpdate(Collection<DBObjectViewModel> myRecipe)
        {
            string CS = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + SelectedFile + ";" + "Extended Properties='Excel 12.0 Xml;HDR=YES'";
            string name;
            decimal? amount;
            decimal? price;
            using (OleDbConnection connection = new OleDbConnection(CS))
            {
                    connection.Open();
                    foreach (var dbObject in myRecipe)
                    {
                        name = dbObject.Name;
                        amount = dbObject.Quantity - dbObject.Amount;
                        price = (dbObject.Price/dbObject.Quantity)*amount;
                        OleDbCommand command = new OleDbCommand("UPDATE [" + "Arkusz1$" + "] SET ilość = @a, cena = @p WHERE nazwa = @i", connection);
                        command.Parameters.Add(ReturnDBParameter("a", amount));
                        command.Parameters.Add(ReturnDBParameter("p", price));
                        command.Parameters.Add(new OleDbParameter("i", name));
                        command.ExecuteNonQuery();
                    }
            }
        }

        OleDbParameter ReturnDBParameter(string parameterName, decimal? value)
        {
            OleDbParameter oleDbParameter = new OleDbParameter(parameterName, Convert.ToDouble(Math.Round(value.Value, 2)));
            oleDbParameter.DbType = DbType.Double;
            return oleDbParameter;
        }

    }
}
