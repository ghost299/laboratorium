﻿using System;

namespace Laboratorium.Model
{
    [AttributeUsage(AttributeTargets.Property)]
    public class DBAttribute : Attribute
    {
        public string Name { get; private set; }

        public DBAttribute(string name)
        {
            Name = name;
        }
    }
}
