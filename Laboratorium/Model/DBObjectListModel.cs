﻿using System.Collections.Generic;
using Laboratorium.ViewModel;

namespace Laboratorium.Model
{
    public class DBObjectListModel
    {
        private static List<DBObjectViewModel> _dBObjectList = new List<DBObjectViewModel>();
        public List<DBObjectViewModel> DBObjectList
        {
            get { return _dBObjectList; }

            set { _dBObjectList = value; }
        }
    }
}
