﻿using System.Collections.ObjectModel;
using Laboratorium.ViewModel;

namespace Laboratorium.Model
{
    public class IngredientListModel
    {
        private static ObservableCollection<DBObjectViewModel> _ingredientList = new ObservableCollection<DBObjectViewModel>();
        public ObservableCollection<DBObjectViewModel> IngredientList
        {
            get { return _ingredientList; }

            set { _ingredientList = value; }
        }
    }
}
