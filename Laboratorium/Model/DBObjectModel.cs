﻿namespace Laboratorium.Model
{
    public class DBObjectModel
    {
        [DBAttribute("Nazwa")]
        public string Name //nazwa składnika pobrana z DB (nie może być null)
        { get; set; }

        [DBAttribute("cena")]
        public decimal? Price //cena składnika pobrana z DB (może być null)
        { get; set; }

        public decimal Amount //wybrana w datagrid2 ilość składnika
        { get; set; }

        [DBAttribute("ilość")]
        public decimal? Quantity //ilość składnika pobrana z DB (może być null)
        { get; set; }

        public decimal? Value //wartość PLN ilości wybranego składnika 
        { get; set; }

        public bool Is_Selected //sprawdzenie na ktorej liście znajduje się obiekt
        { get; set; }
    }
}
