﻿using System.Collections.ObjectModel;
using Laboratorium.ViewModel;

namespace Laboratorium.Model
{
    public class RecipeListModel
    {
        private static ObservableCollection<DBObjectViewModel> _RecipeList = new ObservableCollection<DBObjectViewModel>();
        public ObservableCollection<DBObjectViewModel> RecipeList
        {
            get { return _RecipeList; }

            set { _RecipeList = value; }
        }
    }
}
