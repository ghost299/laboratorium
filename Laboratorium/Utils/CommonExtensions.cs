﻿using System;
using System.Data.SqlClient;
using System.Data.OleDb;

namespace Laboratorium.ViewModel
{
    public static class CommonExtensions
    {
        public static decimal SafeGetDecimal(this SqlDataReader reader, int colIndex)
        {
            if (!reader.IsDBNull(colIndex))
                return reader.GetDecimal(colIndex);
            else
                return 0;
        }

        public static decimal SafeGetDecimal(this OleDbDataReader reader, int colIndex)
        {
            if (!reader.IsDBNull(colIndex))
                return reader.GetDecimal(colIndex);
            else
                return 0;
        }

        public static decimal SafeGetValue(this OleDbDataReader reader, int colIndex)
        {
            if (!reader.IsDBNull(colIndex))
                return Convert.ToDecimal(reader.GetDouble(colIndex));
            else
                return 0;
        }

        public static bool Contains(this string source, string cont, StringComparison compare)
        {
            return source.IndexOf(cont, compare) >= 0;
        }
    }
}
