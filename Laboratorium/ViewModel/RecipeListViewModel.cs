﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Windows;
using Laboratorium.Model;
using Laboratorium.Providers;
using Microsoft.Win32;

namespace Laboratorium.ViewModel
{
    public class RecipeListViewModel: ObservableObject, IAmountChangeListener
    {
        RecipeListModel recipeListModel = new RecipeListModel();
        DBObjectListModel dbObjectListModel = new DBObjectListModel();
        IngredientListModel ingredientListModel = new IngredientListModel();
        IProvider dataProvider = new OleProvider();

        public ObservableCollection<DBObjectViewModel> RecipeList
        {
            get { return recipeListModel.RecipeList; }

            set { recipeListModel.RecipeList = value; }
        }

        private decimal? _sum = 0;
        public decimal? Sum
        {
            get { return _sum; }
            set
            {
                _sum = value;
                RaisePropertyChanged("Sum");
            }
        }

        public void Add(IList selectedItems)//Dodaje obiekty do grid2
        {
            if (selectedItems != null)
            {
                foreach (DBObjectViewModel item in selectedItems)
                {
                    recipeListModel.RecipeList.Add(item);
                    item.Is_Selected = true;
                    item.AddListeners(this);
                }
            }
        }

        public void Remove(IList selectedItems)//Usuwa zaznaczone obiekty z grid2
        {
            if (recipeListModel.RecipeList.Count != 0)
            {
                List<DBObjectViewModel> copySelectedItems = new List<DBObjectViewModel>();
                foreach (DBObjectViewModel item in selectedItems)
                {
                    copySelectedItems.Add(item);
                    item.Is_Selected = false;
                }
                foreach (DBObjectViewModel item in copySelectedItems)
                {
                    recipeListModel.RecipeList.Remove(item);
                }
            }
            UpdateSum();
        }

        public void Clear()//Czyści grid2
        {
            foreach (var item in recipeListModel.RecipeList)
            {
                item.Is_Selected = false;
            }
            recipeListModel.RecipeList.Clear();
            UpdateSum();
        }

        public void Save()//Zapisuje zawartośćgrid2 do pliku .txt
        {
            StreamWriter streamWriter = null;
            MessageBoxResult messageBoxResult = MessageBox.Show("Po zapisaniu pliku ilość składników w bazie zostanie zmieniona. Czy zapisać plik?", "", MessageBoxButton.YesNo);
            if (messageBoxResult == MessageBoxResult.No) return;
            SaveFileDialog saveFile = new SaveFileDialog();
            saveFile.Filter = "plik|*.txt";
            saveFile.DefaultExt = ".txt";
            bool? ifOpen = saveFile.ShowDialog();
            if (ifOpen == true)
            {
                streamWriter = File.CreateText(saveFile.FileName);
                streamWriter.WriteLine("Lista składników: ");
                foreach (var item in recipeListModel.RecipeList)
                {
                    streamWriter.WriteLine("{0}\t{1:C}\t\t Ilość składnika: {2}", item.Name.PadRight(40), item.Price, item.Amount);
                    //TODO: item.Is_Selected = false;
                }
                streamWriter.WriteLine("Suma: {0:C}", Sum);
            }
            if (streamWriter != null)
                streamWriter.Close();

            dataProvider.DBUpdate(recipeListModel.RecipeList);//Update składników w bazie
            recipeListModel.RecipeList.Clear();
            ingredientListModel.IngredientList.Clear();//TODO: dubluje skladniki niewybrane po zapisie innego skladnika do txt
            UpdateSum();
            dataProvider.DBSelectAll(ingredientListModel.IngredientList);
            dbObjectListModel.DBObjectList.AddRange(ingredientListModel.IngredientList);
        }

        public void UpdateSum()
        {
            Sum = 0;
            if (recipeListModel.RecipeList.Count != 0)
            {
                foreach (var item in recipeListModel.RecipeList)
                {
                    if (item.Quantity == 0) continue;
                    Sum += (item.Price/item.Quantity)*item.Amount;
                }

            }
            Sum = Math.Round((decimal)Sum, 2);
        }

    }
}
