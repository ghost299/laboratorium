﻿namespace Laboratorium.ViewModel
{
    public interface IAmountChangeListener
    {
        void UpdateSum();
    }
}
