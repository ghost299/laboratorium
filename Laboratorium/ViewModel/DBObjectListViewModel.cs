﻿using System.Collections.Generic;
using System.Data.OleDb;
using System.Windows;
using Laboratorium.Model;
using Laboratorium.Providers;
using Microsoft.Win32;

namespace Laboratorium.ViewModel
{
    public class DBObjectListViewModel: ObservableObject
    {
        private DBObjectListModel dbObjectListModel = new DBObjectListModel();
        private IngredientListModel ingredientListModel = new IngredientListModel();
        private RecipeListModel recipeListModel = new RecipeListModel();
        private IProvider dataProvider = new OleProvider();

        private string selectedFile;
        public string SelectedFile
        {
            get { return selectedFile; }
            set
            {
                selectedFile = value;
                RaisePropertyChanged("SelectedFile");
            }
        }

        public List<DBObjectViewModel> DBObjectList
        {
            get { return dbObjectListModel.DBObjectList; }

            set { dbObjectListModel.DBObjectList = value; }
        }

        public void SelectFile()
        {
            try
            {
                OpenFileDialog openFile = new OpenFileDialog();
                openFile.Filter = "plik|*.xlsx";
                openFile.DefaultExt = ".xlsx";
                if (ingredientListModel.IngredientList.Count > 0)
                {
                    MessageBoxResult messageBoxResult = MessageBox.Show("Po załadowaniu nowego pliku zostaną usunięte wszystkie wybrane składniki. Czy załadować nowy plik?", "", MessageBoxButton.YesNo);
                    if (messageBoxResult == MessageBoxResult.No) return;
                }
                if (openFile.ShowDialog() == true)
                {
                    LoadFile(openFile);
                }
            }

            catch (OleDbException)
            {
                MessageBox.Show("Wybrany plik jest uszkodzony lub zawiera niepoprawne wartości.");
            }
            catch (System.Exception se)
            {
                MessageBox.Show(se.ToString());
            }
        }

        private void LoadFile(OpenFileDialog openFile)
        {
            recipeListModel.RecipeList.Clear();
            dbObjectListModel.DBObjectList.Clear();
            SelectedFile = null;
            dataProvider.SelectedFile = openFile.FileName;
            dataProvider.DBSelectAll(ingredientListModel.IngredientList);
            dbObjectListModel.DBObjectList.AddRange(ingredientListModel.IngredientList);
            SelectedFile = openFile.FileName;
        }

    }
}
