﻿using System.Collections.Generic;
using System.Windows;
using Laboratorium.Model;

namespace Laboratorium.ViewModel
{
    public class DBObjectViewModel: ObservableObject
    {
        private DBObjectModel dbObjectModel = new DBObjectModel();

        private List<IAmountChangeListener> _amountListeners = new List<IAmountChangeListener>();

        public DBObjectViewModel(string name, decimal? price, decimal? quantity)
        {
            Name = name;
            Price = price;
            Quantity = quantity;
        }

        public DBObjectViewModel()
        {
        }

        public string Name 
        {
            get { return dbObjectModel.Name; }
            set { dbObjectModel.Name = value; }
        }

        public decimal? Price
        {
            get { return dbObjectModel.Price; }
            set { dbObjectModel.Price = value; }
        }

        public decimal Amount
        {
            get
            {
                return dbObjectModel.Amount;
            }
            set
            {
                if (value < 0)
                {
                    MessageBox.Show("Ilość składnika nie może być ujemna.");
                }
                else if (value <= Quantity)
                {
                    dbObjectModel.Amount = value;
                    NotifyAmountChanged();
                    RaisePropertyChanged("Value");
                }
                else
                {
                    MessageBox.Show("Nie można wybrać większej ilości składnika niż jest na stanie.");
                }
            }
        }

        public decimal? Quantity 
        {
            get { return dbObjectModel.Quantity; }
            set { dbObjectModel.Quantity = value; }
        }


        public decimal? Value
        {
            get
            {
                if (Quantity != 0)
                {
                    return (Price/Quantity)*Amount;
                }
                else
                {
                    return 0;
                }
            }
        }

        public bool Is_Selected
        {
            get { return dbObjectModel.Is_Selected; }
            set { dbObjectModel.Is_Selected = value; }
        }

        private void NotifyAmountChanged()
        {
            foreach (var AmountListeners in _amountListeners)
            {
                AmountListeners.UpdateSum();
            }
        }

        public void AddListeners(IAmountChangeListener listener)
        {
            _amountListeners.Add(listener);
        }

    }
}
