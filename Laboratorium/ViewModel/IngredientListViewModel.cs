﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Laboratorium.Model;

namespace Laboratorium.ViewModel
{
    public class IngredientListViewModel:ObservableObject
    {
        DBObjectListModel dbObjectListModel = new DBObjectListModel();
        IngredientListModel ingredientListModel = new IngredientListModel();

        public ObservableCollection<DBObjectViewModel> IngredientList
        {
            get {return ingredientListModel.IngredientList;}
            set { ingredientListModel.IngredientList = value; }
        }

        private static string searchString;
        public string SearchString 
        {
            get { return searchString;}
            set
            {
                searchString = value;
                RaisePropertyChanged("SearchString");
            }
        }

        public void Filter()//Wyszukuje obiekt po nazwie
        {
            if (searchString == null)
            {
                Reload();
                return;
            }
            ingredientListModel.IngredientList.Clear();
            foreach (var item in dbObjectListModel.DBObjectList.Where(u => u.Name.Contains(searchString, StringComparison.OrdinalIgnoreCase) && u.Is_Selected == false))
            {
                ingredientListModel.IngredientList.Add(item);
            }
        }

        public void Reload()//Uaktualnia listę
        {
            ingredientListModel.IngredientList.Clear();
            foreach (var item in dbObjectListModel.DBObjectList.Where(u => u.Is_Selected == false))
            {
                ingredientListModel.IngredientList.Add(item);
            }
            SearchString = null;
        }

        public void Add(IList selectedItems)//Dodaje obiekty do grid1
        {
            if (selectedItems != null)
            {

                foreach (DBObjectViewModel item in selectedItems)
                {
                    item.Amount = 0;
                    ingredientListModel.IngredientList.Add(item);
                    item.Is_Selected = false;
                }
            }
        }

        public void Remove(IList selectedItems)//Usuwa zaznaczone obiekty z grid1
        {
            if (ingredientListModel.IngredientList.Count != 0)
            {
                List<DBObjectViewModel> copySelectedItems = new List<DBObjectViewModel>();
                foreach (DBObjectViewModel item in selectedItems)
                {
                    copySelectedItems.Add(item);
                    item.Is_Selected = true;
                }
                foreach (DBObjectViewModel item in copySelectedItems)
                {   
                    ingredientListModel.IngredientList.Remove(item);
                }           
            }

        }
    }
}
