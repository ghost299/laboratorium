﻿using System.Windows;
using Laboratorium.Providers;
using Laboratorium.ViewModel;

namespace Laboratorium
{
    public partial class MainWindow : Window
    {
        #region Property

        private DBObjectListViewModel dbObjectList = new DBObjectListViewModel();
        public DBObjectListViewModel DbObjectList
        {
            get { return dbObjectList; }
            set { dbObjectList = value; }
        }
        private IngredientListViewModel ingredientList = new IngredientListViewModel();
        public IngredientListViewModel IngredientList
        {
            get { return ingredientList; }
            set { ingredientList = value; }
        }
        private RecipeListViewModel recipeList = new RecipeListViewModel();
        public RecipeListViewModel RecipeList
        {
            get { return recipeList; }
            set { recipeList = value; }
        }

        private IProvider dataProvider = new OleProvider();
        #endregion

        public MainWindow()
        {
            InitializeComponent();
            dataGrid.ItemsSource = ingredientList.IngredientList;
            dataGrid2.ItemsSource = recipeList.RecipeList;
            dataProvider.DBSelectAll(ingredientList.IngredientList);
        }

        private void SearchButtonClick(object sender, RoutedEventArgs e)//Szukaj
        {
            ingredientList.Filter();
        }

        private void AddButtonClick(object sender, RoutedEventArgs e)//Dodaj
        {
            recipeList.Add(dataGrid.SelectedItems);
            ingredientList.Remove(dataGrid.SelectedItems);
        }

        private void DeleteButtonClick(object sender, RoutedEventArgs e)//Usun
        {
            ingredientList.Add(dataGrid2.SelectedItems);
            recipeList.Remove(dataGrid2.SelectedItems);
        }

        private void ClearGrid1ButtonClick(object sender, RoutedEventArgs e)//Wyczysc wyniki wyszukiwania w grid1
        {
            ingredientList.Reload();
        }

        private void ClearGrid2ButtonClick(object sender, RoutedEventArgs e)//Wyczysc grid2
        {
            recipeList.Clear();
            ingredientList.Filter();
        }

        private void SaveButtonClick(object sender, RoutedEventArgs e)//Zapisz
        {
            recipeList.Save();
        }

        private void SelectFileButtonClick(object sender, RoutedEventArgs e)//Wybierz Excel
        {
            dbObjectList.SelectFile();
        }

    }
}
